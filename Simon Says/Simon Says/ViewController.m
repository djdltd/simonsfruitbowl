//
//  ViewController.m
//  Simon Says
//
//  Created by Danny Draper on 23/11/2015.
//  Copyright © 2015 Danny Draper. All rights reserved.
//

#import "ViewController.h"

#define TOP    0
#define BOTTOM 1
#define LEFT   2
#define RIGHT  3

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *topImage;

@property (weak, nonatomic) IBOutlet UIImageView *leftImage;

@property (weak, nonatomic) IBOutlet UIImageView *bottomImage;

@property (weak, nonatomic) IBOutlet UIImageView *rightImage;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (weak, nonatomic) IBOutlet UIButton *startGameButton;

@property (weak, nonatomic) IBOutlet UIButton *highScoresButton;
@property (weak, nonatomic) IBOutlet UIButton *gameOverButton;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *helpImage;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"View loaded.");
    
    // Initialise Audio
    _soundarray = [NSMutableArray new];
    
    _allowuserinput = false;
    _state = 0;
    _highlightrepeat = 1;
    _currentrepeat = 0;
    _gamespeed = 0.7;
    //NSTimer *t = [[NSTimer alloc] interval: 1 target: self selector:@selector(onTick:) userInfo:nil repeats:YES];
    
    [self initialiseButtons];
    [_scoreLabel setHidden:true];
    [_statusLabel setHidden:true];
    [_helpImage setHidden:true];
    [_closeButton setHidden:true];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToRightWithGestureRecognizer:)];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToUpWithGestureRecognizer:)];
    
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToDownWithGestureRecognizer:)];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapButton:)];
    
    _menuGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuButton:)];
    
    NSMutableArray *pressTypes = [NSMutableArray new];
    [pressTypes addObject:[NSNumber numberWithInt:UIPressTypeMenu]];
    _menuGesture.allowedPressTypes = pressTypes;
    
    
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    
    
    
    [self.view addGestureRecognizer:swipeRight];
    [self.view addGestureRecognizer:swipeLeft];
    [self.view addGestureRecognizer:swipeUp];
    [self.view addGestureRecognizer:swipeDown];
    [self.view addGestureRecognizer:tapGesture];
    //[self.view addGestureRecognizer:_menuGesture];
    
    //NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
    
    _sequence = [NSMutableArray new];
    _usersequence = [NSMutableArray new];
    [_gameOverButton setHidden:true];
    
    [_backgroundImage setAlpha:0.5];
    
}

- (void) userSwipe:(int)button
{
    if (_allowuserinput == true) {
        NSLog(@"Swiped Button: %i", button);

        [_usersequence addObject:[NSNumber numberWithInt:button]];
        _gameovercorrectoption = [self userSequenceCorrect];
        
        if (_gameovercorrectoption != -1) {
            // Game over
            _allowuserinput = false;
            [_gameOverButton setHidden:false];
            [self fadeGameover];
            _highlightrepeat = 3;
            [self highlightButton:_gameovercorrectoption];
            
        } else {
            
            
            [self highlightButton:button];
            
            if ([_usersequence count] == [_sequence count])
            {
                [_scoreLabel setText:[NSString stringWithFormat:@"Score: %lu", (unsigned long)[_sequence count]]];
                _allowuserinput = false;
                
                // Increase speed of game the better the users play
                if ([_sequence count] > 4) {
                    _gamespeed = 0.6;
                }
                
                if ([_sequence count] > 6) {
                    _gamespeed = 0.5;
                }

                if ([_sequence count] > 9) {
                    _gamespeed = 0.4;
                }
                
                if ([_sequence count] > 14) {
                    _gamespeed = 0.3;
                }
                
                [_statusLabel setText:@"Watch me..."];
                [self delayNewSequence];
            }
        }
    }
}

- (void) delayNewSequence
{
    _delaytimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(newSequenceTick:) userInfo:nil repeats:NO];
}

-(void)newSequenceTick:(NSTimer *)timer
{
    [self newSequence];
}

- (int) userSequenceCorrect {
    
    for (int s=0;s<[_usersequence count];s++) {
        
        NSNumber *_compnum = [_sequence objectAtIndex:s];
        NSNumber *_usernum = [_usersequence objectAtIndex:s];
        
        if ([_compnum intValue] != [_usernum intValue]) {
            return [_compnum intValue];
        }
    }
    
    return -1;
}


-(void)slideToRightWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [self userSwipe:RIGHT];
}

-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [self userSwipe:LEFT];
}

-(void)slideToUpWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [self userSwipe:TOP];
}

-(void)slideToDownWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [self userSwipe:BOTTOM];
}

- (void)tapButton:(UITapGestureRecognizer *)gestureRecognizer {
    if (_helpImage.hidden == false) {
        [_helpImage setHidden:true];
        [_startGameButton setHidden: false];
        [_highScoresButton setHidden:false];
        [self.view removeGestureRecognizer:_menuGesture];
    }
}

- (void)menuButton:(UITapGestureRecognizer *)gestureRecognizer {
    NSLog (@"Menu button pressed!");
    
    if ([_helpImage isHidden] == TRUE) {
        if ([_scoreLabel isHidden] == TRUE) {
            // We are at the main menu, exit the app
            exit(0);
        } else {
            // We are in a game, go back to main menu
            _allowuserinput = false;
            [_playtimer invalidate];
            [_delaytimer invalidate];
            [self fadeMainMenu];
            [self.view removeGestureRecognizer:_menuGesture];
        }
    } else {
        // Help image shown, return to main menu
        [_helpImage setHidden:true];
        [_startGameButton setHidden: false];
        [_highScoresButton setHidden:false];
        [self.view removeGestureRecognizer:_menuGesture];
    }
}

- (void) initialiseButtons
{
    [_topImage setBackgroundColor:[UIColor redColor]];
    [_bottomImage setBackgroundColor:[UIColor greenColor]];
    [_leftImage setBackgroundColor:[UIColor yellowColor]];
    [_rightImage setBackgroundColor:[UIColor blueColor]];
    
    [_topImage setAlpha:0.0];
    [_bottomImage setAlpha:0.0];
    [_leftImage setAlpha:0.0];
    [_rightImage setAlpha:0.0];
}

- (void) fadeGameover
{
    _gameoverfadeamount = 0.0;
    [_gameOverButton setAlpha:0.0];
    _gameoverfadetimer= [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(gameoverTick:) userInfo:nil repeats:YES];
}

-(void)gameoverTick:(NSTimer *)timer
{
    [_gameOverButton setAlpha:_gameoverfadeamount];
    
    _gameoverfadeamount += 0.05;
    
    if (_gameoverfadeamount > 3) {
        [_gameoverfadetimer invalidate];
        _gameoverfadeamount = 1;
        [self fadeMainMenu];
    }
}

- (void) fadeMainMenu
{
    [_startGameButton setAlpha:0.0];
    [_highScoresButton setAlpha:0.0];
    [_startGameButton setHidden:false];
    [_highScoresButton setHidden:false];
    [_scoreLabel setHidden:true];
    [_statusLabel setHidden:true];
    
    _backgroundfadeamount = 1.0;
    _mainbuttonfadeamount = 0.0;
    
    _menufadetimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(menufadeTick:) userInfo:nil repeats:YES];
}

-(void)menufadeTick:(NSTimer *)timer
{
    if (_backgroundfadeamount > 0.5) {
        _backgroundfadeamount-=0.05;
        [_backgroundImage setAlpha:_backgroundfadeamount];
    }
    
    [_startGameButton setAlpha:_mainbuttonfadeamount];
    [_highScoresButton setAlpha:_mainbuttonfadeamount];
    [_gameOverButton setAlpha:_gameoverfadeamount];
    
    if (_mainbuttonfadeamount > 1) {
        [_menufadetimer invalidate];
        [_gameOverButton setHidden:true];
    }
    
    _mainbuttonfadeamount+=0.05;
    _gameoverfadeamount-=0.05;
}

- (void) fadeButton:(int)button {
    
    _currentrepeat = 0;
    _fadeamount = 0.0;
    _fadedirection = 0;
    _fadebutton = button;
    
    _fadetimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(fadeTick:) userInfo:nil repeats:YES];
}

-(void)fadeTick:(NSTimer *)timer
{
    if (_fadebutton == TOP) {
        [_topImage setAlpha:_fadeamount];
    }

    if (_fadebutton == BOTTOM) {
        [_bottomImage setAlpha:_fadeamount];
    }
    
    if (_fadebutton == LEFT) {
        [_leftImage setAlpha:_fadeamount];
    }
    
    if (_fadebutton == RIGHT) {
        [_rightImage setAlpha:_fadeamount];
    }
    
    if (_fadedirection == 0) {
        _fadeamount += 0.2;
        
        if (_fadeamount > 1) {
            _fadedirection = 1;
        }
    }
    
    if (_fadedirection == 1) {
        _fadeamount -= 0.2;
        
        if (_fadeamount < 0) {
            _fadedirection = 0;
            
            _currentrepeat++;
            
            if (_currentrepeat == _highlightrepeat) {
                [_fadetimer invalidate];
            }
            
        }
    }
    
}


- (void) highlightButton:(int)button {
    
    if (_fadetimer != nil) {
        [_fadetimer invalidate];
    }
    [_topImage setAlpha:0.0];
    [_bottomImage setAlpha:0.0];
    [_leftImage setAlpha:0.0];
    [_rightImage setAlpha:0.0];
    
    [self playSound:button];
    
    [self fadeButton:button];
}

-(void)onTick:(NSTimer *)timer
{
    //NSLog(@"timer has ticked.");
    

    if (_state == 0) {
        [self highlightButton:TOP];
    }
    
    if (_state == 1) {
        [self highlightButton:BOTTOM];
    }
    
    if (_state == 2) {
        [self highlightButton:LEFT];
    }
    
    if (_state == 3) {
        [self highlightButton:RIGHT];
    }

    _state++;
    
    if (_state > 3) {
        _state = 0;
    }
    
    [self newSequence];
}

- (void)resetSequence
{
    _sequence = [NSMutableArray new];
}

-(int)getRandomNumberBetween:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}

- (void)newSequence
{

    int randomNumber = [self getRandomNumberBetween:0 to:3];
    [_sequence addObject:[NSNumber numberWithInt:randomNumber]];
    
    _usersequence = [NSMutableArray new];
    
    
    _playindex = 0;
    _playtimer = [NSTimer scheduledTimerWithTimeInterval:_gamespeed target:self selector:@selector(playTick:) userInfo:nil repeats:YES];
    
}

-(void)playTick:(NSTimer *)timer
{
    NSNumber *currentNumber = [_sequence objectAtIndex:_playindex];
    [self highlightButton:[currentNumber intValue]];
    
    _playindex++;
    
    if (_playindex == [_sequence count]) {
        _allowuserinput = true;
        [_playtimer invalidate];
        [_statusLabel setText:@"Your turn..."];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonOne:(id)sender {
    NSLog(@"button one clicked");
    //[_topImage setBackgroundColor:[UIColor blueColor]];
    
    _soundarray = [NSMutableArray new];
    [self resetSequence];
    //[self newSequence];
    [self delayNewSequence];
    _highlightrepeat = 1;
    _gamespeed = 0.7;
    [_scoreLabel setHidden:false];
    [_scoreLabel setText:@"Score: 0"];
    [_statusLabel setHidden:false];
    [_statusLabel setText:@"Watch me..."];
    [_backgroundImage setAlpha:1];
    [_startGameButton setHidden:true];
    [_highScoresButton setHidden:true];
    
    [self.view addGestureRecognizer:_menuGesture];
}

- (IBAction)buttonTwo:(id)sender {
    NSLog(@"button two clicked");
    //[_topImage setBackgroundColor:[UIColor redColor]];
    
    [_helpImage setAlpha:0.7];
    [_helpImage setHidden:false];
    
    [_startGameButton setHidden:true];
    [_highScoresButton setHidden:true];
    [self.view addGestureRecognizer:_menuGesture];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Touch has begun!");
}

- (void) playSound:(int)note
{
    /* Use this code to play an audio file */
    NSString *soundFilePath;
    
    if (note == 0) {
        soundFilePath = [[NSBundle mainBundle] pathForResource:@"Note" ofType:@"m4a"];
    }
    
    if (note == 1) {
        soundFilePath = [[NSBundle mainBundle] pathForResource:@"Note2" ofType:@"m4a"];
    }
    
    if (note == 2) {
        soundFilePath = [[NSBundle mainBundle] pathForResource:@"Note3" ofType:@"m4a"];
    }
    
    if (note == 3) {
        soundFilePath = [[NSBundle mainBundle] pathForResource:@"Note4" ofType:@"m4a"];
    }
    
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:soundFilePath] error:nil];
    [_soundarray addObject:audioPlayer];
    audioPlayer.numberOfLoops = 0;
    [audioPlayer prepareToPlay];
    [audioPlayer play];
}

- (IBAction)gameOverButtonPressed:(id)sender {

}

- (IBAction)closeButtonPressed:(id)sender {
    [_helpImage setHidden:true];
    [_closeButton setHidden:true];
    [_startGameButton setHidden:false];
    [_highScoresButton setHidden:false];
}

@end
