//
//  ViewController.h
//  Simon Says
//
//  Created by Danny Draper on 23/11/2015.
//  Copyright © 2015 Danny Draper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController {
    int _state;
    int _gameovercorrectoption;
    int _highlightrepeat;
    int _currentrepeat;
    float _fadeamount;
    float _gameoverfadeamount;
    float _backgroundfadeamount;
    float _mainbuttonfadeamount;
    float _gamespeed;
    
    int _fadedirection;
    int _fadebutton;
    int _playindex;
    bool _allowuserinput;
    NSTimer *_fadetimer;
    NSTimer *_gameoverfadetimer;
    NSTimer *_menufadetimer;
    NSTimer *_playtimer;
    NSTimer *_delaytimer;
    NSMutableArray *_sequence;
    NSMutableArray *_usersequence;
    NSMutableArray *_soundarray;
    UITapGestureRecognizer *_menuGesture;
}



@end

