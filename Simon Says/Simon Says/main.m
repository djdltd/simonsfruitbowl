//
//  main.m
//  Simon Says
//
//  Created by Danny Draper on 23/11/2015.
//  Copyright © 2015 Danny Draper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
