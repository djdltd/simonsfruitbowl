//
//  AppDelegate.h
//  Simon Says
//
//  Created by Danny Draper on 23/11/2015.
//  Copyright © 2015 Danny Draper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

